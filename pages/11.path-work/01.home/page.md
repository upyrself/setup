---
title: 'What is Pathwork and energy work?'
media_order: 'penal-gland.jpg,meditationPinelGlandStimulation.jpg,mind-power-copy.jpg,everythingisconnected.jpg'
date: '10:07 05-12-2020'
hide_page_title: true
show_sidebar: false
hide_git_sync_repo_link: false
---

Do not mistake what I am teaching here to be related to some form of religion. This is not religion. It is your life.

You work with energy everyday, every moment. Your thoughs are energy. Energy is vibration, frequency. Your words are vibration, energy. Although you may not be aware of it, you create every moment, and every experience through your personal energy. No one can see what you see. You have to describe it to them. Most of the time your thoughts are being hi-jacked. The marketing is about researching how to manipulate people to buy what they don't need. You are taught to create a need, make the person feel their is a reward to have it and they will buy it.

Your thoughts are powerful and they create your material life. You are responsible for manifesting that tree just as much as you are the troubles in your mind. Manifestation, prana energy allways follow thought. Energy is neither positive or negative. It just is. How it is used depends on your thoughts and actions.

It is a common practice amongst all species to fit in and to be like others. In animals or other species humans call what they do instinct.

BUT IT IS MORE THAN THAT.

Humans consider anything not human, as not having intelligence. So we step on insects, chop down trees, pollute our water ways and yet
all of these things are connected. They talk to one another. The tree the air, earth, water, sun, moon - they are talking and exchanging energy.

![energy" class="left](mind-power-copy.jpg)The ant responds to the sun, moon, changes in the earth and etc. Ducks fly to specific destinations based upon the position of the planets, sun and moon.
When a storm is coming, birds disappear. Have you ever asked where do they go?

Everything in nature operates in their original state except humans. An ant operates as ants have for thousands of years.
Humans on the other hand have evolved and changed their natural state by removing himself from nature. We can no longer communicate
with these universal energies on a conscious level. So we find substitutes. Exercise programs replaced hard physical labor,
television replaces meditation by allowing a picture on a screen to teach us to think, see and act. The mind doesn't know
the difference between television and real life.

We live in homes, protect ourselves from the rain, earth (wearing shoes), snow, sun, insects, dirt and, etc. We sanitize our hands,
bathe often to was off dirt, cover ourselves for protection from the sun, we totally cut ourselves off from nature.

![connection-to-all" class="right](everythingisconnected.jpg)Go camping or participate in an outdoor hiking trip and answer what is your level of tolerance to nature? Did you find it difficult
to deal with the bugs?

The earth, rocks, dirt, sand and, etc contains energy that the subtle body needs. It isn't enough to eat from the earth.
You have to allow the earth to charge your body by touching it. The same goes for the air or the wind. The wind is air energy moving at a faster pace and that energy
at any speed calms your subtle systems. Natural water from streams, oceans and lakes will do the same things, heal and recharge the body.

Path-work has nothing to do with worship, religion or anything practice out-side energy work and body energy as a whole.

Path-work is about you working with energy, through meditation, to discover fully who you by correcting past selves.

Some benefits of making this journey is that you can visit your past self and repair damage done to your self through bad experiences.
Making this corrections to damage done to your child person changes your current life. So the goal and objective of path-work has to do with
fixing the past, so that those bad experiences no longer cause damage in your present day.

It can also include linking experiences together to form meaning. For example, I wanted to know why I continue to fall into the
habit of helping other people and sometimes putting them before my own interests. When I begin meditating, my intentions or focus was
on getting an answer. So I focused on the answer.

I arrived at elementary school and saw myself standing there holding my sister, Debbie's, hand as she cried afraid to go
into her Kindergarden class. I was trying to calming her down and encouraged her to see how fun it was. She calmed down. I heard a womans voice and looked up at a person I assumed was her teacher, she told me she  found my behavior extrordinary for a child my age. I didn't understand what that meant then but somehow my inner self did and it liked it. I was quickly carried to many other stories in my life where I was the hero the person who helped people. I visited myself as I jumped between Tyrone Davis and Derrick Jones. Tyrone was being encouraged to fight Derrick by Michael and a couple other guys Tyrone hangs with. Because Tyrone would not stop I hit him once in the face and he gave up.

![meditation" class="left](meditationPinelGlandStimulation.jpg)I joined my elementary schools safety patrol because I saw how important it was for kids to cross the street safely, plus I liked getting out of class early and drinking hot chocolate in the morning. My dad put me in the cub scouts and that was really fun. Boy I love the outdoors as a kid. I felt really connected to nature I tolerated bugs more than. The cub scouts helped introduce me to how much fun you can have with nature and boy scouting allowed me to nuture my leadership skills. I spent so much time helping others complete their merit badges I neglected to get my final badge to become eagle scout. I did a similar thing In the military, my squad member was struggling with defusing land mines. I walk them through it over and over and they passed. But when my turn came, I made one mistake and didn't max the test. In college, my courses were usually boring, so I'd control the dialog between the teacher and us by asking interesting questions in a way that made everyone else participate. I learned so much more that way and so do the other students. At work, I always always finding better ways of doing things, and I try to push others who feel less of themselves to be better. In ministry, I dedicated my entire life to experiencing life's pain and sufferings learning step by step how it happens and the process it takes to overcome. What I learn teach help others. When I write or teach I like to break the information down into its smallest parts and share it in such a way that everyone sees themselves
in it, sees what is possible and how they can use the information to further their understanding and bring more clarity into their life.

That this is who I am. My whole life has been about creating change and sharing that with others. I have always been the type of person who would step in front of the bus to stop it, if I felt it would save a life. I'm the person who would walk 10 miles with a person if it would encourage them to live. All at the expense of my own life.

That's power, knowing yourself and what your path work is makes you a person. Now that I know about that part of me. I can build my life up from that position and reject what doesn't represent that path.
When a person asks me to become a part of such and such, I can with confidence tell them No and why.

The opposite happens to a person who doesn't know themselves. They reach for everything else except the answers inside of them. You will never be happy living this way. You will grow more and more angry at the world because of this unhappiness. When something positive happens, a mind like this will transmutate it into something negative. They are drawn to everything negative. They become the harmful bacterial flowing through the vains of this material life. Jesus says, let the tares grow with the wheat. Harvest will take care of the separation.

For you in difficult situations, you can use energy or spellwork to cast or repell negative energy and there are things you can create to keep them from your life or every coming into contact negative energy. What do you do when you encounter hateful people. Use energy, your practices with energy work should also include learning defensive skills. If you doubt what I am saying, then you need to read books on electronics. 

The reason a radio can pick up various frequencies and a broadcast station can add vibrations to that frequency channel for you to listen at great distances is because energy can be manipulated. In electronics you use components like resisters (resist electrical current), capacitors (are collectors of energy - once fully charged - releases that electrical charge all at once), transistors (are like gates, where traffic - the flow of electrical current, can be redirected). The components used in electronics are all materials found on the earth. Certain stones or rock carry electical energy that can be measured, while others give off no known signal. Yet when the rock is broken down into its smallest component, energy, the same energy flowing through your body exists in the rock.

Some rocks repell while others attrack like energy. Light reflects off colors, light is vibration, light vibrates at different frequences. The frequency at which a light vibrates determine its color. Colored lights years ago, in the 60-80's, were painted. Today, you can purchase a clear light when turned on exhibits color. The point is this. Stop allowing society to guide your path.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YEJ2qryXcIQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You may be the type of person who love reading books and to you, that is your passion. I don't want you to stop, but I want you to visit yourself and ask why are you doing that? If it is out of some fear of the unknown, you need to find your past self and replace that experience with a healthy one. The outcome is to turn the way you gain knowledge in-ward. Your brain contains everything you've ever experienced, rather you paid attention to the details around you or not. Your brain remembers every detail. You and only you have access to those memories and those memories feed your intentions in life. They help you decide what is right and wrong for you.

You may be the type of person who feels a commitment to your belief's and that maintaining them is important to you. Don't stop. Path work does not include destroying some belieft to create another belief. That's not how the human experience works. You simply cannot forget. You might not know how to access those memories, but they are still there. It have been written too and modified over the years as you gained more experiences and knowledge. For example, the first time you felt pain and understood how it happened, you formed an opinion about that moment and for the rest of your life your story about the little booboo on the knee, has become something quite different. But it's the same story. Your response on that first day, is the same response you give today, except you may have adjusted your response so that you won't be embarrassed, because, well nobody likes throwing a tandum in front of other adults, right?

So keep your faith, but reach for it from the inside. Your inner temple has to somehow use those teachings to create this unique person who is now walking in their power. Since everything is connected, your religion or faith is not in question here. What is in question is why? How did you arrive at becoming what you claim verbally to be. Because if that is not what your energy self's intentions are your out of syn. Your energy is being blocked. Your chakras which are responsible for transmitting energy from the invisible world into this material world is restricted or blocked. Signs of blocked chakras are problems in specific areas of your body.

When the Third Eye Chakra is blocked a person's sleep patterns will be interrupted. Thinking clearly or reasonably is a problem and their perspectives are usually not inline with what is true. A persons whose third eye chakra is blocked cannot be reasoned with and often sees everyone as a threat. How do you correct this chakra? Darkness. The pineal gland which is sensitive to light needs to sit in darkness for awhile in meditation until the inner light can be seen. This practice doesn't take away from your religious experience, but adds to it.