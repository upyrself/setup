---
title: 'Header Image'
media_order: mind-power-copy.jpg
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
routable: false
visible: false
---

File to be used for the header image above the menubar.
