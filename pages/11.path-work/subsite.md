---
title: 'What is pathwork and energy work?'
subsite_list_description: 'Pathwork is about discovering who you really are, undoing damage done to your past self by visiting and talking to your past self and changing negative experiences. This practice automatically creates confidence and elevates your personal power, improving your life situations.<div class="mx-auto mt-10"><a href="path-work/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: true
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
visible: false
page-inject:
    processed_content: true
---

