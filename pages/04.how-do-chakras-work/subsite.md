---
title: 'How do Chakras work?'
subsite_list_description: 'Each chakra has a separate and distinct role in regulating the physical body. <div class="mx-auto mt-10"><a href="/how-do-chakras-work/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: false
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
visible: false
page-inject:
    processed_content: true
---

