---
title: 'The Kundalini'
date: '10:07 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

**The Kundalini is known as goddess power or serpent power.**

The Kundalini is located at the base of the spine coiled upward like a serpent and possess immense power..

The practice of awakening the Kundalini, once awakened, becomes a gateway, a key to psychic powers and even enlightenment. Anyone with a passion for spiritual development should learn and practice Kundalini awakening because it will take you to higher spiritual levels. The acquisition of psychic powers is merely incidental to the process of Kundalini awakening.

![kundalini" class="right](TheKundalini-2.gif)Kundalini works closely with **prana**. Prana is the pervading energy that exists inside you and is all around you.

The Kundalini is often awakened by drawing more prana into the location of the root or base chakra. When you Kundalini is awakened, a strong rush of prana surges through the body.

An awakened Kundalini promotes good health by regulating and correcting blood pressure, stress levels, fight and even cure diabetes and other diseases and physical problems. Other benefits are:
* Clarity of thought
* Increased focus
* Attention
* Mental power

There are different methods of awakening your Kundalini, therefore, you should not count on a single exercise to help awaken your Kundalini. Rotate between exercises to find one that works for that moment.

It should be noted that awakening the Kundalini is more a mental effort and practice. You should be expected to engage in long hours of meditation or physical exercises that help awaken your Kundalini.

<div class="mx-auto mt-10"><a href="benefits-before-the-awakening" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
