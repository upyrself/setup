---
title: 'The Kundalini'
subsite_list_description: 'Kundalini is conceptualized as a coiled-up serpent at the base of the spine upwards. Kundalini is a subtle energy form that can be measured like ordinary nerve circulation. <div class="mx-auto mt-10"><a href="the-kundalini/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: true
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
visible: false
page-inject:
    processed_content: true
---

