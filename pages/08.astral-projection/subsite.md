---
title: 'Astral projection'
hide_from_subsite_list: false
subsite_home: home
published: false
date: '21:29 06-12-2020'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

I want to experience out-of-body travel as quickly as possible. The most effective way is a universal technique called cycles of indirect techniques. This is the most effective way to obtain a phase experience (OBE) because it is performed upon awakening. At that time the human brain is physiologically rather close to dreams or are still in them.

The cycles of indirect technique consists of me making attempts to separate immediately upon awakening.

Then if that is unsuccessful, then cycle through the next technique, for 1 minute until one of them works.

Then it is possible to separate from the body adgain and dive into a lucid dream. Usually 1 to 5 cycles properly performed are all I need to obtain results.

The technique

This technique works:

- awakening from a nap
- in the middle of the night

But our focus or concentration will be on this strategy - the deferred method. The deferred method allows for numerous attempts in a single day. This increases the probability for one to occur in 1 to 3 days.