---
title: 'How do Auras Work?'
subsite_list_description: 'Each layer of the aura correlates to a different element of the physical, mental, spiritual and emotional health. They can interact with one another to influence your overall health.<div class="mx-auto mt-10"><a href="/how-do-auras-work/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: false
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

