---
title: Sidebar
routable: false
visible: false
cache_enable: false
---

## Workshop Facilitator
Dr. Wm (Ezek) King

[safe-email autolink="true" icon="envelope"]billaking@willerie.com[/safe-email]

[fa icon="twitter"][@billaking](https://twitter.com/billaking)

## Twitter Feed
[twitter url="https://twitter.com/billaking" text="Tweets by @billaking" height="600"]
