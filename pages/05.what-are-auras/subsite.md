---
title: 'What are Auras?'
subsite_list_description: 'There''s an energy field of different layers and colors enveloping your physical body. <div class="mx-auto mt-10"><a href="/what-are-auras/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: false
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
visible: false
page-inject:
    processed_content: true
---

