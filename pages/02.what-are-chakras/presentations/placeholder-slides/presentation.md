---
title: 'Use the arrows below to navigate the slides.'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
hide_title: false
display_custom_links: false
---

[.text: alignment(center)]

# What are Chakras?

> Each chakra has a distinct position, color and serves a different purpose. The chakras regulate and influence every part of the body.”
-- Dr. Wm King

---

[.background-color: #FFFFFF]

![fit](https://getgrav-grav.netdna-ssl.com/user/pages/01.tour/_easy-to-use/001-dashboard.png)

---

[youtube]https://www.youtube.com/watch?v=SvWzquy8vBg[/youtube]

---

# Topics to Explore
1. Topic One  
2. Topic Two   
3. Topic Three  

---

# Placeholder Slides

### Topic One

---

# Placeholder Slides

### Topic Two

---

# Placeholder Slides

### Topic Three

---

# Summary
1. Topic One  
2. Topic Two   
3. Topic Three  

---
