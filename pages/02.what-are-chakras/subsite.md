---
title: 'What are Chakras'
subsite_list_description: 'Chakras are invisible energy points that connect your spiritual body to your physical body. <div class="mx-auto mt-10"><a href="what-are-chakras/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
media_order: chakra-balancing-and-restoration-course-banner.png
published: true
hide_page_title: true
show_sidebar: false
hide_git_sync_repo_link: true
visible: false
page-inject:
    processed_content: true
---

