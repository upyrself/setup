---
title: '3rd (throat) Chakra'
media_order: Vishuddha-Chakra-Header.jpg
date: '10:07 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

![](Vishuddha-Chakra-Header.jpg)

**Vishuddha, the fifth chakra, means “purification.” **

The throat chakra is located in the throat. The color of the throat chakra is blue. It contains a circular in shape that is made up of sixteen purple and smoky gray petals and inside a blue triangle with a white circle in the center. 

The center circle refers to a full moon. The moon itself symbolize the invisible etheric energy that is associated with dreaming. 

The throat chakra influences the following:

* speech
* communication
* creative expression. 

When the throat chakra is heathy, clear and open; lettings things go is easy and the ability to express owns view with confidence.

This chakra helps us to not dwell on the past, present, or future anxieties, and supports gains in wisdom from life experiences. This chakra helps your speech flow freely and helps you live in a state of healthy detachment, making it easier to experience powerful spirit dreams. The “energy” associated with the throat chakra is akin to our astral body, and at night it’s believed that this etheric body leaves the body to dream.

When this chakra is blocked the ability to express yourself is limited. A person with a weak throat chakra is easily filled with guilt and shame over the past, they fear the future, and struggle to live in the present moment.

The roots of the word Vishuddha are “visha” and “shuddhi.” Visha means “poison” and shuddhi means “purification.” Energy comes in pairs and carries its opposite. Together they create balance.

Therefore, when this chakra is closed, it becomes poisonous to the body.

**Resonance** is this chakra’s biggest influence. When the throat chakras energy flows freely, you feel confident in who you are and easily share your voice with the world around you.

This chakra has a seed mantra sound associated with it, “Ham.” It’s pronounced “hahmm” rather than ham, like the pig. 

If you feel your throat chakra is blocked, repeating 'seed mantra' pronounced "hahmm" during meditation helps purify it. 

Other means of clearing out the throat include things like doing the yoga asanas, head stands. Singing during meditation is also a healthy activity for the throat chakra.

The throat chakra is associated with dreams. Dream Yoga is a very important part of Tibetan Buddhist practice, and it is seen as an important stepping stone on the path of transcendence from the endless cycles of birth and death. Many Eastern religions believe that we exist in an elaborate dream that only appears to be “real.”

Working with dreams is an excellent way of tapping into your past and rewriting your feelings about difficult moments in your life. This practice can alter how you see your life when not in lucid dreams or walking in your atral body. By developing lucid dreaming, or waking up in a dream while the body remains asleep you can manipulate will and manifest. Once lucid dreaming can be induced at will, it is possible to direct the course of dreams for spiritual and material benefit. 

Mastering dreams will prepare you for the intense tidal wave of visual and sensory manifestations which occur just after death. Buddhist monks believe that we become entranced with these post-mortem manifestations and are drawn back into a new incarnation. However, if we can transcend these illusory, dreamlike images then we have a chance of breaking the endless cycle of death and rebirth.

Within the throat chakra lie the beginnings of dream yoga practice. There are many visualizations, mantras, yogic, breathing, and other practices that rely on an active and healthy throat chakra for their successful completion.

<div class="mx-auto mt-10"><a href="/what-are-chakras/third-eye" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
