---
title: '2nd (3eye) Chakra'
media_order: Ajna-Chakra.jpg
date: '10:07 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

![](Ajna-Chakra.jpg)

**The next chakra is called Ajna, which means “command.”** 

The third eye chakra is located between the brows, just above the eyes, and ,at the back of the head, just at the top of the spine.

It's color is indigo blue. The Ajna chakra has two petals on either side. 

These petals symbolize a pair of **nadis** running up either side of the body that connect near the Ajna chakra. They end at the nostrils.

The word Ajna means self-command. Self-command is achieved by overcoming the illusion of duality. It is also the deep surrendering to the command or guidance of the guru, the inner temple. Ultimately, your own liberated self is the guru.

When the Ajna chakra is healthy and activated, it influences the spiritual development of a person where duality is overcome. Therefore, Ajna chakra governs spiritual awakening. Its keyword feature is illumination. When the Ajna chakra is bright and unblocked attachment to the material, temporal world fades and a unified mental state emerges. When the third eye is awakened, it is possible to quickly burn through past karma, detach from the illusory sufferings it brings, and finds true inner peace. An activated throat chakra suggests desirable or necessary course of actions to achievement of a high level of self-purification, while the third eye chakra’s activation suggests or yields transcendence.

The third eye chakra influences the following: 
* Psychic abilities, or siddhis, 
* A coming alive.
* Mental powers
 
The chakras do not correlate to the physical body but rather are pieces of a spiritually connected energetic transformation of consciousness. In the process of spiritual awakening the body is transformed, making the person whole. 

Many people desire psychic ability and it is a completely natural phenomenon. Everyone is born with these abilities; it is only a matter of practice to activate them. 

When the Ajna chakra is spontaneously awakened, psychic abilities can come all at once and the experience can be overwhelming. 

In order to move past the third eye chakra and into the crown chakra you must overcome all attachments to the siddhis (Siddhis are material, paranormal, supernatural, or otherwise magical powers, abilities, and attainments that are the products of yogic advancement through sādhanās such as meditation and yoga.). 

Psychic abilities can have a powerful effect on your life experience and letting go of what may feel like omniscient powers can be a difficult task.

Ligh, sunlight and the light of conscious illumination is the third eye chakra’s driving force.

<div class="mx-auto mt-10"><a href="/what-are-chakras/crown" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
