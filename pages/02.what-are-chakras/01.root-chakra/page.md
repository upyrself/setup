---
title: '7th (base) Chakra'
media_order: Muladhara-Chakra-Header.jpg
date: '10:06 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

![](Muladhara-Chakra-Header.jpg)

**In Sanskrit, the word muladhara means “root support.”**

The root or base chakra is located at the base of the spine. The color of the root chakra is red.

To unblock energy in your chakras, you focus on filling them with light and letting go of any past density that you may hold there.

The color perception of a chakra involves determining how bright and light your chakra is. Since chakras are made of pure energy we want our chakras to be vibrant, bright, clear, luminous and not dull.

The root chakra has four deep red petals.

Muladhara (root chakra) influences the following:
* Survival
* Security

The root chakra is about support, stability and being rooted.

The root chakra is called the base chakra because it sits at the base of the spine. It's responsible for how you relate to and feel supported materially in the world. A clear root chakra will bring vitality to your feet and legs. Energy flowing through the root chakra will be powerful and plentiful.

When in balance, muladhara resonates security, stillness, and stability.

The root chakra helps you feel safe in the world by making sure that your basic physical needs for shelter, food, water, sleep, clothing, and material comfort are met. Being grounded means that you are rooted and present in your body and on the planet earth.

<div class="mx-auto mt-10"><a href="/what-are-chakras/sacral" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
