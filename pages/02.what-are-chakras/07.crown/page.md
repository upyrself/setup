---
title: '1st (Crown) Chakra'
media_order: Sahasrara-Chakra.jpg
date: '10:07 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

![](Sahasrara-Chakra.jpg)

The crown chakra is the last and seventh energy center in most traditional chakra systems.

**In Sanskrit, the word Sahasrara means “thousand-petalled.”**

The crown chakra has a infinite number of petals. The color of the crown chakras is violet or white. It is located on at the top of the head where your soft spot used to be when we were babies. ![" class="right](crown-chakra-location-1.jpg) Where the petals of the other chakras are typically drawn pointing up, the crown points down.

The crown chakras process of making known one's thoughts or feelings represents enlightenment, pure awareness, and an ability to escape from the endless cycles of birth and death from which most of us suffer. But for most people the crown chakra is also connected with inspiration and creativity, similar to the third eye chakra.

The consciousness and awareness of the crown chakra is beyond duality, grasping for temporal things like love, success, and so on. A person with an activated crown chakra glows, literally. Life is lived fully aware, from moment to moment, desiring nothing and appreciating every new moment and the experiences as they come. It’s a state of consciousness wholly foreign to most human.However, it represents the goal of the human experience and is possible to achieve in a single lifetime.

When the crown chakra is out of balance the mind can become poisoned and can lead to mental problems, insanity, the god complex, and many unfortunately high number of modern spiritual gurus.

Because everything is energy, food, liquids, air, fire, humans, plants, soil, moon, sun, universe all have energy altering abilities. Everything becomes important to the development of chakras and the healthy expression of kundalini life-force energy.

Listen to your inner temple and follow it. Be warned there traditions that shape our understanding about the energy fields are opininated. You must walk your own path, in doing so, you will find you. Taking advice is good. But no one else knows you as well as you know yourself. Some gurus will tell you to avoid things like cannabis or other things they label drugs. They say because cannibis can affect the auric energy field. That's true, but so does food and what we drink. Be smart and enjoy your journey. My advice is this, if your taking cannabis for spiritual reasons benefit you continue to do so. You are the same you when you come out of your dreams, or visualizations. Rather you take cannabis or not, the same properties that exists in the cannabis planet also exists in your human nervous system. The cannabionds in your system help fight disease, manage pain and many other things. All the cannabis plant does is add additional cannabionds into your blood stream to help support the bodies systems, and helps fight and slow the spread of disease.

But be warned, practicing while high can lead to some amazing or scary experiences. Cannabis has the ability, for example to blow open the third eye chakra and can allow a person who is unprepared to experience psychic phenomenon. That experience can become uncomfortable and make you afraid, it can be wonderful, or it can lead to feelings of being gods or goddesses.

The crown chakra is about illumination, essence that connects us to the divine, and to the entire cosmos. The crown chakra can also awaken you to allow you to remember connections of the past, present and future.

Because the crown chakra is beyond all express, represents the void between things, the pause between and inhale and exhale (of creation), it has no seed mantra, or sound.

This chakra represents the moment between all things. That position allows the crown chakra to connect all things together. It connects things in-between the spaces; the invisible to the visible; that which is eternal to the temporaral.

<div class="mx-auto mt-10"><a href="/what-are-chakras/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Home</a></div>
