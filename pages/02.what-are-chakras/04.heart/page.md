---
title: '4th (heart) Chakra'
media_order: Anahata-Chakra-Header.jpg
date: '10:07 05-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

![](Anahata-Chakra-Header.jpg)

**The Sanskrit translation of the word anahata is “unstruck”**

The heart chakra is located in the center of the chest. The color of the heart chakra is bright emerald green. 

The brighter and more full of light the color is, the healthier the chakra.

The heart chakra has twelve deep green petals surrounding a six-pointed star.

The heart chakra "Anahata" influences the following:
* rules love, 
* breath, 
* balance, 
* relationships, 
* and unity. 

When this chakra is inbalance, it is responsible for your higher energies of love and compassion. It governs matters of romance and friendship and heart connection. When your heart chakra’s need are met, you feel loved, cared for, and you love yourself.

This chakra’s most influential force is equilibrium, which means it is concerned with you having a state of emotional balance and calmness. A healthy heart chakra lets you know that you are never alone. It also is responsible for helping you to extend caring to others and yourself.

<div class="mx-auto mt-10"><a href="/what-are-chakras/throat" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
