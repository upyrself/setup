---
title: 'Make Your Own Melatonin'
subsite_list_description: 'Like a lot of people supplementing with melatonin to help with sleep, I was not aware that I can make my own melatonin--in my brain!  In fact, you make your own melatonin in the pineal gland.<div class="mx-auto mt-10"><a href="diy-sleep/home" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Get Started</a></div>'
hide_from_subsite_list: false
subsite_home: home
published: true
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
visible: false
page-inject:
    processed_content: true
---

