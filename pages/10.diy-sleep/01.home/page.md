---
title: 'Make Your Own Melatonin'
media_order: 'penal-gland.jpg,meditationPinelGlandStimulation.jpg'
date: '10:07 05-12-2020'
hide_page_title: true
show_sidebar: false
hide_git_sync_repo_link: false
---

## A Do It Yourself Sleep Solution


As a software developer I solve a lot of user problems and sometimes I take those thoughts
to bed keeping me up at night.

Like a lot of people supplementing with melatonin to help with sleep, I was not aware that
my habits was preventing me from making my own melatonin--in my brain! In fact, you make melatonin in the pineal gland.

## The Duality of the human body.

![penal-gland" class="right](penal-gland.jpg)The human body has two of almost everything.
* Legs
* Arms
* Lungs
* Ears
* Eyes
* Nostrils
* etc...

As it relates to the human brain, there are two of everything in the brain except for the pineal gland, you only have one. The pineal gland is called the third eye chakra or as Jesus calls it the light of the body.

> Jesus says, "The light of the body is the eye. If therefore thine eye be single, thy whole body shall be full of light. But if thine eye be evil, thy whole body shall be full of darkness. If therefore the light that is in thee be darkness, how great is that darkness!

You can read [about the third eye here](/what-are-chakras/home).

The Third Eye Chakra sits center and just above of your eyebrows. It is the same spot that pentecostal preachers anoint
with oil and lay hands when praying for people.

The pineal gland plays an important role in meditation (MEDITATION: to engage in mental exercise for the purpose of reaching a heightened level of spiritual awareness) and reaching enlightenment ( is when your desires and sufferings are gone).
When you go in and out of consciousness you are activating/deactivating the pineal gland by falling asleep (unconscious) or waking up (conscious).

It is said that the pineal gland is MOST ACTIVE twice during everyone's life.
1. The moment you arrive into this world (e.g. becoming conscious)
2. At the moment of death (e.g. becoming unconscious).



The pineal gland works in many ways like a doorway between the spirit and physical world. The pineal gland is responsible for your conscious and unconscious states. Although it is deep in the brain, it is sensitive to light energy.

![penal-gland-meditation" class="left](meditationPinelGlandStimulation.jpg)Bad sleep habits are a common problem for the pineal gland's inability to release melatonin and syncing your bodies natural clock which is responsible for putting you to sleep. Your eyes are one way to send chemical and electrical messages to the pineal gland. When your eyes register less light, meaning it is getting dark; the
pineal gland is signaled to start producing melatonin.

With modern conveniences of LED, fluorescent lights, computers, mobile phones and TV screens your eyes receive a lot of light at night. As the body is winding down, the pineal gland is expecting a message to put you to sleep. Since there is too much light, the body and the pineal gland becomes out of sync;
and if the pineal gland doesn't the get messages that it’s time to start producing melatonin because it is still light. Health problems can become a serious issue if this pineal gland is not healthy. See the [Third Eye Chakra](/what-are-chakras/third-eye) and it's responsibility and power.

Insomnia is related to poor sleep habits, depression, anxiety, lack of exercise, chronic illness, or certain medications. one outcome from the pineal gland becoming ill. The pineal gland is your gateway to the spiritual world.

### Changing your habits

One solution is to avoid bright lights (use candles) for at least 1 hour before bed. Doing this can help the eyes to send the right message to the pineal gland so that it can create melatonin. If you struggle with sleep then you need to get your body and mind in sync and that practice will take time.
