---
title: 'Header Image'
media_order: what-is-aura-image.png
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
routable: false
visible: false
---

File to be used for the header image above the menubar.
