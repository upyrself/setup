---
title: 'Patterns and vibrations'
date: '22:28 11-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---

What do patterns and vibrations have to do with Magick or energy work?

Thought is energy, invisible energy. It is said that prana follows thought. Thought energy can manifest itself materially in many ways,
for example, as vibrations made with the breath or physical motion; as in getting something done. Because energy is neither positive, nor negative.
No experience is negative. How it becomes negative or positive has to do with how the subconscious mind handles the information.

Because the subconscious mind blocks, then filters or evaluates new information; creating new patterns of thought is simply impossible, unless you can get past
the subconscious minds filters you can will continue to manifest using your current patterns of thought.

So, how do you cause a new creative pattern?

Stay tuned! we will get to that next.

<div class="mx-auto mt-10"><a href="everything-is-vibration" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>

