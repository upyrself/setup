---
title: 'Three Areas of Concern'
date: '22:28 11-12-2020'
hide_page_title: false
show_sidebar: false
hide_git_sync_repo_link: false
---


| The conscious | The subconscious | The Will |
| ------ | ------ | ------|
| The conscious mind only sees facts in the light of present subconscious creative patterns, which is why some people are unable to see a fact glaring at them. It is contrary to their deep beliefs. | The subconscious mind is the powerhouse. It creates around us all that it believes to be true, according to the instructions we give it. Everything around you here and now has been created for you by your subconscious mind and has been attracted to you like a magnet. It is impossible to stop this creation process. Even when you leave Earth life, it will carry on creating for you and serving you. For that is its purpose: to attract to you all that you need, It does not reason; it just accepts instructions that are believed to be in your best interest. | Before the subconscious changes its creative pattern, it has to be convinced that the new instruction is valid. This is where the will and the conscious mind come into the picture. The will has to get past the barriers of the conscious mind. No matter how strong the will is to change something, it has to convince the conscious mind before that new instruction can reach the subconscious mind.  |

When you were very young, while your critical faculties were wide open for learning, your subconscious mind was trying
hard to do what your will wished, which often followed with discipline. Most of what you learned at that time was presented to you as facts, and you were expected to accept them without question.

These facts, true or not, became your truth, and help you make decisions. The subconscious mind use filters slow new information down so that it can validate
that information against what is known by you. The subconscious mind seeks to reinforce and substantiate deep-seated beliefs.

When this happens, new creative patterns of thinking are impossible, or are slow to come.

<div class="mx-auto mt-10"><a href="patterns-and-vibrations" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
