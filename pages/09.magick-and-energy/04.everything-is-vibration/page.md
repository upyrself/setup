---
title: 'Everything is vibration'
media_order: 'earthRotation.png,NVov6VA.gif,vibratory patterns in nature.jpg,seamless-black-white-pattern-stones-260nw-607288055.png,leopards-patterns.PNG,ezgif-4-ddd084977dfe.gif,plant-cells.jpg,space-sounds.mp3,Plants-Growing-to-Jazz.gif'
date: '19:32 30-12-2020'
hide_page_title: true
show_sidebar: false
hide_git_sync_repo_link: true
---

Still in Draft: 

Everything in nature is made of invisible energy. Invisible energy that vibrates at different frequencies. The frequency at which a thing vibrates manifests distinct patterns and behaviors. The pattern and behaviors or properties of the thing vibrating is responsible for its manifestation in the material world.

Think about it. How do you break a glass without touching it? How do you cut a diamond? How do you break up concrete into pieces? Vibrations at a higher frequency than the solid object. Thought is energy, how do you influence a person? words! Words are vibrations heard by the ears and transmutted into electrical or chemical energy the brain can process.

Light for example is energy. With the right materials light can be harnest to create other energies. You can, for example, use light to see more clearly in a dark space. You can convert light to energy for cooking food (microwave) or as heat to warm your subtle body. Solar panels are examples of harnessing light. Cooper wire is used in electronic circuits because it already contains electrical energy. When no electrical current is applied to the copper wire the number of electrons and protons are equal. It is said that the copper wire has no potential. The moment you introduce 1 electron or proton to the copper wire it will attempt to rebalance. The rebalancing requires that, if one (1) electron was introduced to the circuit, one electron must leave the wire. If you continue to add more electrons you create a flow of electrical energy.  The act of adding electrons causes the other electrons and protons to vibrate.

The point is, everything is made of energy, which means everything in nature can be used to create and manifest other things; including your words. 

Coal may look like a rock, but it contains properties, dna patterns and energy exists elsewhere in nature. You can use the energy of the coal and combine that with fire to create other types of energy or combine the coal with various surfaces and you can draw with it. Coal contains the same energy that exists in fire. The vibratory patterns that formed the coal allow a fire's energies to bond with the coal, and transform it into other energies. What's left of the coal is a different form altogether.

Everything is connected to one another. The energy patterns existing in one object exists in all objects. It is at the level of frequency or vibration that distinctions are made and new patterns and behaviors are formed and manifested to create differences between you, a tree, wind, fire, water and etc.

The vibration at which an object vibrates creates unique patterns that your natural senses pickup as lighting, sound, solids, liquids and so on.

A single snow flake contains the pattern of every other snow flake before it. If you examine the DNA of the snow flake you will find its DNA in every other object existing on the earth and in space. To sum it up, that snow flake contains vibratory energy that formed the pattern, that we see as snow. Have you every asked where the snow flake came from? It started as invisible energy. The right conditions converted that energy into a new pattern. What's so interesting is that it was done in mid air and descended downward until it landed or was transformed into a new pattern such as rain. Yet, the rain still contains the patterns that existed in the snow flake.

Energy is neither positive or negative. It just is. It becomes one or the other when new patterns are formed. 

Words for example are energy vibrations. Because words are energy, they are neither positive or negative. The word **Nigger**, used during slavery against dark skinned people, and accompanied with pain and suffering, was meant to influence a negative response from the hearer. I dare you to respond, Nigger.

It was said in the bible by Jesus, that nothing without a man, entering into the man defiles the man, meaning outside his mind, body or spirit, nothing can corrupt a person. It means that nothing on the planet or beyond is evil or bad, nor is it good or pure. It just is. But when a person processes it, they change it.

Energy is balanced as positive and negative (as in the copper wire example). The moment the vibration of words enter into the ears of a person, it is processed, transmutted and transformed into patterns of thinking. What follows after that are responses, actions, or behaviors. These are patterns created by the mind of the person. Your mind is responsible for everything happening to you.

Your reactions are a reflection of your thought patterns. Another person may have a different reaction to the word **Nigger** because they do not have the same collective thought patterns that you possess. That's why Jesus said, "the Light of the body is the eye..." How you see a thing transmutates that vibratory energy into thought patterns, and the output is energy in various forms, such as a physical or verbal reaction. You just converted one (1) energy type into another. Magickal right?

You look outside your window and see leaves all over your grass. Your reaction to the leaves depends entirely on how you see the leaves in your mind. If you see them out of place, you might respond by removing them. If you see them and they aren't a problem, you may do nothing at all. It does not mean you are lazy; it does not mean your response was wrong. Depending on where you live others might see it differently. What you do as a response to their thought patterns is really up to you.

When you observe a snow flake you are seeing yourself and the same eyes that observes the snow flake is looking back at you. You and the snow flake are one and the same. You see it and it is you. Your eyes are converting light, shadows and contrasts into electrical and chemical vibrations (impulses) in the brain. As a child you didn't judge what a snow flake was. You were experiencing it. As an adult you either try to harness it for skying, or some other sport, or you ignore it altogether.

--- I stopped here : Edits ---


Let's talk for a moment about this (subtle) body.

Scientists have discovered, if you take a metal plate and put sand on the plate and add sound vibrating at various frequencies patterns appear in the sand on the plate that mimic nature.

Add corn starch to water and do the exact same test and the corn starch will appear to be alive.

Space itself vibrates and has its own unique sound. 

There is sounds in space, this was captured by NASA - 
<audio controls autoplay>
  <source src="https://upgradeyourself.billaking.com/magick-and-energy/everything-is-vibration/space-sounds.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

If you gave read my articles on Chakra energy you know that your physical body is not you only. Its the manifestation of your invisible self. Inside of you there's a energy field maintaining your physical vibrations which keeps you in the earthly body.

When your internal energy field is dim energy is hindered and affects individual Chakras, sickness in the body occurs because the chakras are vibrating incorrectly. This keeps the various parts of your body in a unhealthy state.

To heal them you need to use nature to do it. The Root Chakra for example gets is vibratory patterns and energy from the earth.

Soil, trees, rocks, certain herbs Chakra. anything that comes from the earth can help restore lost energy because they contain certain energies that support the root chakra.

You can release their energies using different vibrational techniques. Mantras, ceremonies, casting, spell work, meditation, praying, tinctures, oils containing specific ingredients, topical or ingesting.


!["planet-rotation class="right](ezgif-4-ddd084977dfe.gif)**The planets do not rotate in a circle as originally taught in science classes in the early 19th century.** With new advances in science we can now observe our solar system and planets from space. What is interesting is ancient people always knew that the planets in the solar system doesn't rotate in a perfect circle. 

The universe uses vibrations and the spiral pattern in everything in nature. When seed growth is recorded on video in a time lapse, the plant can be seen as if it were dancing.

![](Plants-Growing-to-Jazz.gif)

### Other Patterns in nature

Everything in nature is connected. The patterns in nature exists in everything.

The photo below shows how salt, poured on a metal surface with a speaker attached can, at different vibrational frequencies produce patterns found in nature, such as the cells in our body, the nervous system, coats on animals, barks on the tree and the earth.

![](vibratory%20patterns%20in%20nature.jpg)

A Leopard's coat contains patterns found in human and cells of plants.
!["kundalini class="right](leopards-patterns.PNG)
![](plant-cells.jpg)
!["kundalini class="right](seamless-black-white-pattern-stones-260nw-607288055.png)









<div class="mx-auto mt-10"><a href="patterns-and-vibrations" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>