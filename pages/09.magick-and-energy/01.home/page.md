---
title: 'Magick and Your Energy Body'
date: '10:07 05-12-2020'
hide_page_title: true
show_sidebar: false
hide_git_sync_repo_link: false
---

# What is Magick

!["kundalini class="right](TheKundalini-2.gif)Magick is different from magic. Magic is seen on the stage, illusions, sleight of hand, or in religious gatherings they use trickery and should not be confused with energy work, Magick.

Magick, a true Magickians will not openly display his Magick, because it's personal and unique to them.

A Magickian work with energy, prana that exists in all things. Everything is affected by energy. Magcikians learn to manipulate them and use them as an aid to a better life and understanding of that life more clearly.

True Magick comes from your individuality. Only you can know what is true for you and what you will.

Real Magick is highly individualized and understanding how it works within the light of reason is important as you grow, so that you learn to think and not accept unproven facts and superstitions.

As it relates to reason, all thought produce results.

By thought, I am speaking of deliberate thought activated by the will and not daydreaming, or normal conscious that which is analytical, critical and makes comparisons for evaluation.

Willed thought produce results, physical results, and **thought use energy to create**.

There are three areas of the mind which concerns us. We will look at them on the next page.

<div class="mx-auto mt-10"><a href="three-areas-of-concern" class="mx-auto btn btn-outline-dark "><i class="fa fa-play fa-fw"></i> Next</a></div>
